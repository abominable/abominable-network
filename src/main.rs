use std::{io::{Error, Read, Write, ErrorKind}, net::{Ipv4Addr, SocketAddrV4, TcpListener, TcpStream}, path::Path, fs::File};

use chrono::Utc;

fn main() -> Result<(), Error> {

    listen_for_connections(25566)?;

    Ok(())
}

fn listen_for_connections(port: u16) -> Result<(), Error> {
    let loopback = Ipv4Addr::new(127, 0, 0, 1);
    let socket = SocketAddrV4::new(loopback, port);
    let listener = TcpListener::bind(socket)?;
    let listening_port = listener.local_addr()?;
    println!("Listening on {listening_port}. Exit the program to stop listening.");
    
    loop {
        let (mut tcp_stream, incoming_addr) = listener.accept()?;
        println!("Connection made to {incoming_addr}.");
        
        // Ignore errors so that bad input streams are simply skipped (instead of crashing the program).
        _ = handle_input_stream(&mut tcp_stream)?;
    }
}

fn handle_input_stream(tcp_stream: &mut TcpStream) -> Result<(), Error> {
    let mut input = String::new();
    tcp_stream.read_to_string(&mut input)?;
    println!("Received message. Storing.");
    
    //TODO: Store messages that were not stored properly (...wait).
    store_message(input)?;
    
    Ok(())
}

fn store_message(mut message: String) -> Result<(), Error> {
    let utc_time = Utc::now();
    let utc_date_pretty = utc_time.format("%v (%a)");
    let utc_time_pretty = utc_time.format("[UTC] %R");

    let filename = utc_time.format("%F");
    let path_string = format!("leaves/{filename}.txt");
    let path = Path::new(path_string.as_str());
    
    let file_exists = path.is_file();
    let mut file = File::options().append(true).create(true).open(path)?;

    if file_exists {
        message = format!("{utc_time_pretty}\n{message}\n\n");
    } else {
        message = format!("{utc_date_pretty}\n\n{utc_time_pretty}\n{message}\n\n");
    }
    
    file.write(message.as_bytes())?;

    Ok(())
}